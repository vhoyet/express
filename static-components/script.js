window.onload = function() {
    const searchBar = document.getElementById('search-bar')
    searchBar.addEventListener('keyup',function(event){
        if(event.keyCode === 13){
            const content = document.getElementById('content')
            deleteChildren(content)
            event.preventDefault()
            search()
        }
    })

    let selects = document.getElementsByTagName('select')
    let l = selects.length
    for( let i = 0 ; i < l ; i++){
        selects[i].addEventListener('change', search)
    }

    search()
}    

async function search(){
    let selects = document.getElementsByTagName('select')
    let tags = {}
    tags.architecture = selects[0].selectedIndex > 0 ? selects[0].value : null
    tags["PCI"] = selects[1].selectedIndex > 0 ? selects[1].value : null
    tags.core = selects[2].selectedIndex > 0 ? selects[2].value : null
    tags.socket = selects[3].selectedIndex > 0 ? selects[3].value : null
    tags.NUMA = selects[4].selectedIndex > 0 ? selects[4].value : null
    tags.OSDevice = selects[5].selectedIndex > 0 ? selects[5].value : null
    tags["various-memory"] = selects[6].selectedIndex > 0 ? selects[6].value : null
    tags.year = selects[7].selectedIndex > 0 ? selects[7].value : null

    tags.title = document.getElementById('search-bar').value
    tags.title =  tags.title ? tags.title : null
    console.log("https://vhoyet-express.herokuapp.com/tags/" + encodeURIComponent(JSON.stringify(tags)))
    let response = await fetch("https://vhoyet-express.herokuapp.com/tags/" + encodeURIComponent(JSON.stringify(tags)))
    let data = await response.json()

    const content = document.getElementById('content')
    deleteChildren(content)
    data.xml.forEach(xml => {
        const a = document.createElement('a')
        a.href = "https://vhoyet-express.herokuapp.com/xml/" + xml.title
        a.innerHTML = xml.title
        a.classList.add('zoomIn')
        a.classList.add('animated')
        content.appendChild(a)
        content.innerHTML += "<br>"
    });
}

function deleteChildren(element){
    while(element.firstChild)
        element.removeChild(element.firstChild)
}
