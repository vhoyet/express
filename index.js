const express = require("express")
const app = express()
const cors = require('cors')
const testFolder = './xml/'
const fs = require('fs')
const path = require('path')

app.use(cors())

app.get("/", (req, res) => {
  let xml = new Array()
  console.log("ok")
  fs.readdir(testFolder, (err, files) => {
    files.forEach(file => {
      xml.push({ title: file })

      if(files.indexOf(file) == files.length - 1)
        res.send({xml})
    })
  })
})

app.get("/tags/:tags", (req, res) => {
  let req_tags = JSON.parse(req.params.tags)
  let xml = new Array()
  fs.readdir(testFolder,(err, files) => {
    files.forEach(async file => {
      let jsonfile = "./tag/" + file.substring(0, file.length-3) + "json"
      let rawdata = fs.readFileSync(jsonfile);
      let tags = JSON.parse(rawdata);
      if( ( req_tags.title == null ||  file.includes(req_tags.title) ) &&
          ( req_tags.architecture == null || tags.architecture == req_tags.architecture ) &&
          ( req_tags["PCI"] == null || tags["PCI"] == req_tags["PCI"] ) &&
          ( req_tags.core == null || tags.core == req_tags.core) &&
          ( req_tags.socket == null || tags.socket == req_tags.socket ) &&
          ( req_tags.NUMA == null || tags.NUMA == req_tags.NUMA ) &&
          ( req_tags.OSDevice == null || tags.OSDevice == req_tags.OSDevice ) &&
          ( req_tags["various-memory"] == null || tags["various-memory"] == req_tags["various-memory"] ) &&
          ( req_tags.year == null || tags.year== req_tags.year ) )
          xml.push({ title: file })

        if(files.indexOf(file) == files.length - 1)
          res.send({xml}) 
    })
  })  
})

fs.readdir(testFolder, async (err, files) => {
  files.forEach(file => {
    app.get("/xml/" + file, (req, res) => {
      const xml = path.join(__dirname, testFolder + file)
      res.download(xml)
    })
  })
})

const PORT = process.env.PORT || 5000;
app.listen(PORT, function() {
  console.log(`App listening on port ${PORT}`)
})